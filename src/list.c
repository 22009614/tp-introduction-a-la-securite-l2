#include<stdlib.h>
#include<stdio.h>


struct cell_t {
  void* val;
  unsigned long int id;
  struct cell_t* next;
};

typedef struct cell_t* list_t;

list_t list_empty(){
	return NULL;
}

int list_is_empty(list_t l){
	return  (l == NULL);
}

list_t list_push(list_t l, void* x){
	/*INSERTION AU DEBUT*/
	list_t tmp;
	tmp = malloc(sizeof(struct cell_t));
	tmp->val = x;
	tmp->next = l;
	if (list_is_empty (l)){
		tmp->id = 1;
	} else  {
		tmp->id  = l ->id +1;
	}
	return tmp;
}	
	
	
	/*
	 *INSERTION A LA FIN 
	list_t tmp;
	tmp = l;
	unsigned long int idTmp;
	if (list_is_empty (l)){
		l = malloc(sizeof(cell_t));
		l->val = x;
	}
	while (next != NULL){
		tmp = tmp ->next;
		idTmp = id;
	}
	tmp -> val = x;
	tmp -> id = idTmp +1;
	tmp -> next = NULL;
	return tmp;*/


list_t list_tail(list_t l){
	return l -> next;
}

void* list_pop(list_t* l){
/* list_t* l equivaut à struct cell_t**  */
	void* tmp  = (*l)-> val;
	list_t new_l = (*l)-> next;
	free(*l);
	(*l) = new_l;
	return tmp;
}

void* list_top(list_t l){
	return l->val;
}

void list_destroy(list_t l, void (*freeVoid)(void*)){
	while (l -> next != NULL){
		l = l ->next;
		}
	if (!(list_is_empty(l))){
		free(l);
		freeVoid(l->val);
	}
}

// return the found element or NULL
void* list_in(list_t l, void* x, int (*eq)(void*, void*)){
	if (list_empty(l)){return NULL;}
	while (!list_empty(l)) {
		if(eq(l->val, x)){
			return l->val;
		}
		l = l->next;
	}
}

unsigned long int list_len(list_t l){
	return l->id;
}

